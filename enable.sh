#!/bin/sh

normalize() {
    norm_dir_name="`cd "${1}";pwd`"
    echo "${norm_dir_name}"
}

script_dir=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
LXPACK_DIR=`normalize ${script_dir}/`

export PATH=${LXPACK_DIR}/bin:$PATH
export CPATH=${LXPACK_DIR}/include:$CPATH
export LD_LIBRARY_PATH=${LXPACK_DIR}/lib:${LXPACK_DIR}/lib64:${LD_LIBRARY_PATH}
export LD_RUN_PATH=${LXPACK_DIR}/lib:${LXPACK_DIR}/lib64:${LD_RUN_PATH}
export LIBRARY_PATH=${LXPACK_DIR}/lib:${LXPACK_DIR}/lib64:${LIBRARY_PATH}
export PKG_CONFIG_PATH=${LXPACK_DIR}/lib/pkgconfig:${LXPACK_DIR}/lib64/pkgconfig:${PKG_CONFIG_PATH}
#export PYTHONPATH=${LXPACK_DIR}/lib/python2.7/site-packages:${PYTHONPATH}

alias lxpack="source ${LXPACK_DIR}/bin/lxpack.sh"

