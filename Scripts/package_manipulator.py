#!/usr/bin/env python

import package
from package import Package
from console import message, green, red

class PackageManipulator(object):

    def __init__(self, package_list):
        self.packages = []
        for obj in package_list:
            if type(obj) == str:
                self.packages.append(package.get(obj))
            elif isinstance(obj, Package):
                self.packages.append(obj)
            else:
                assert False


class Installer(PackageManipulator):

    def __init__(self, package_list):
        super(Installer, self).__init__(package_list)

    def install(self):
        for p in self.packages:
            self.install_one(p)

    def install_one(self, p):
        Installer(p.get_depends_on()).install()
        if not p.has_src():
            return
        if p.installed():
            return
        message("installing %s" % green(p.name))
        p.extract_src()
        p.pre_install()
        p.install()
        p.post_install()
        p.flags.install()
        message("activating %s" % green(p.name))
        p.activate()


class Uninstaller(PackageManipulator):
    
    def __init__(self, package_list):
        super(Uninstaller, self).__init__(package_list)

    def uninstall(self):
        for p in self.packages:
            self.uninstall_one(p)

    def uninstall_one(self, p):
        from dependency import ReverseDependencyExtractor
        target, depend = ReverseDependencyExtractor([p]).extract()
        for dep in depend:
            Uninstaller([dep]).uninstall()
        if not p.has_src():
            return
        if not p.installed():
            return
        if p.activated():
            message("deactivating %s" % red(p.name))
            p.deactivate()
        message("uninstalling %s" % red(p.name))
        p.uninstall()

        
class Cleaner(PackageManipulator):

    def __init__(self, package_list):
        super(Cleaner, self).__init__(package_list)

    def clean(self):
        for p in self.packages:
            self.clean_one(p)

    def clean_one(self, p):
        if not p.has_src():
            return
        if p.cleaned():
            return
        message("cleaning %s" % green(p.name))
        p.clean()


class Activator(PackageManipulator):

    def __init__(self, package_list):
        super(Activator, self).__init__(package_list)

    def activate(self):
        for p in self.packages:
            self.activate_one(p)

    def activate_one(self, p):
        Activator(p.get_depends_on()).activate()
        if not p.has_src():
            return
        if not p.installed():
            Installer([p]).install()
        if p.activated():
            return
        message("activating %s" % green(p.name))        
        p.activate()


class Deactivator(PackageManipulator):
    
    def __init__(self, package_list):
        super(Deactivator, self).__init__(package_list)

    def deactivate(self):
        for p in self.packages:
            self.deactivate_one(p)

    def deactivate_one(self, p):
        #import dependon
        #for dp in dependon.packager_generator(p.name):
        #    Deactivator([dp]).deactivate()
        if not p.has_src():
            return
        if not p.activated():
            return
        message("deactivating %s" % red(p.name))
        p.deactivate()
