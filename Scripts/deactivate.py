#!/usr/bin/env python

import sys

import package
from console import show_packages, red
from dependency import ReverseDependencyExtractor
from package_manipulator import Deactivator

def show_depend_deactivate(depend):
    depend_deactivated = set()
    for x in depend:
        if package.get(x).activated():
            depend_deactivated.add(x)
    if len(depend_deactivated):
        m = "deactivated because of dependencies"
        show_packages(depend_deactivated, msg=m, color=red)
        m = "Are you sure to deactivate all the dependencies? (yes/no) > "
        if raw_input(m) != "yes":
            sys.exit(0)

def deactivate(args):
    targets, redeps = ReverseDependencyExtractor(args.packages).extract()
    # show_target_deactivate(targets)
    show_depend_deactivate(redeps)
    Deactivator(args.packages).deactivate()

if __name__=="__main__":
    pass
