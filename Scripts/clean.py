#!/usr/bin/env python

import os, sys
import package
from package_manipulator import Cleaner

def clean(args):
    Cleaner(args.packages).clean()

if __name__=="__main__":
    pass
