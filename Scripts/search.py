#!/usr/bin/env python

import os, sys
import package

def search(args):
    for name in package.all_iterator():
        match = True
        for w in args.words:
            if not w in name:
                match = False
                break
        if match:
            print(name)

if __name__=="__main__":
    pass
