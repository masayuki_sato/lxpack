#!/usr/bin/env python

import os

class FileFlag(object):
    
    def __init__(self, path):
        self.path = path

    def set(self):
        os.system("touch %s" % self.path)

    def unset(self):
        os.remove(self.path)

    def isSet(self):
        if os.path.exists(self.path):
            return True
        return False

class FilePassiveFlag(object):
    
    def __init__(self, path):
        self.path = path

    def isSet(self):
        if os.path.exists(self.path):
            return True
        return False
        
class PackageFlags(object):

    def __init__(self, prefix, build_prefix):
        self.prefix = prefix
        self.install_flag = FileFlag(os.path.join(prefix, "LXPACK.installed"))
        self.activate_flag = FileFlag(os.path.join(prefix, "LXPACK.activated"))
        self.dirty_flag = FilePassiveFlag(build_prefix)

    def install(self):
        self.install_flag.set()

    # def uninstall(self):
    #     self.install_flag.unset()

    def installed(self):
        return self.install_flag.isSet()
    
    def activate(self):
        self.activate_flag.set()

    def deactivate(self):
        self.activate_flag.unset()

    def activated(self):
        return self.activate_flag.isSet()

    def cleaned(self):
        return not self.dirty_flag.isSet()
