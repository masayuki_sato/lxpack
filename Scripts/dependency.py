#!/usr/bin/env python

import package
from package_manipulator import PackageManipulator

class DependencyExtractor(PackageManipulator):

    def __init__(self, package_list):
        super(DependencyExtractor, self).__init__(package_list)
        self.package_set = set()
        self.dependency_set = set()
        
    def extract(self):
        for p in self.packages:
            s, d = self.extract_one(p)
            self.package_set = self.package_set | s
            self.dependency_set = self.dependency_set | d
        self.dependency_set -= self.package_set
        return self.package_set, self.dependency_set

    def extract_one(self, p):
        s = set()
        s.add(p.name)
        d = set(p.get_depends_on())
        for x in p.get_depends_on():
            s1, d1 = DependencyExtractor([x]).extract()
            for y in d1:
                d.add(y)
        return s, d

class ReverseDependencyExtractor(PackageManipulator):

    def __init__(self, package_list):
        super(ReverseDependencyExtractor, self).__init__(package_list)
        
    def extract(self):
        redp_set = set()
        name_set = set()
        for p in self.packages:
            name_set.add(p.name)
        for p in package.all_iterator():
            p_str_list, d_str_list = DependencyExtractor([p]).extract()
            for x in name_set:
                if x in d_str_list:
                    redp_set.add(p)
        redp_set -= name_set
        return name_set, redp_set


if __name__=="__main__":
    print(ReverseDependencyExtractor(["curl"]).extract())
