#!/usr/bin/env python

import os, errno

from config import lp2_pkg_install_prefix, lp2_pkg_active_prefix
from console import warn, pkg_msg

class FileManipulator(object):

    def makedirs(self, path):
        try:
            os.makedirs( path )
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def chdir(self, path):
        pkg_msg("Change directory to %s" % path)
        os.chdir( path )

    def link(self, src, dst):
        if os.path.exists(dst):
            pkg_msg("%s already come from %s"
                    % (warn(dst),
                       warn(self.show_package_dst(dst))))
            return
        self.makedirs(os.path.dirname(dst))
        os.symlink(src, dst)

    def show_package_dst(self, dst):
        p = os.readlink(dst)
        p1 = os.path.relpath(p, lp2_pkg_install_prefix)
        p2 = os.path.relpath(dst, lp2_pkg_active_prefix)
        return p1.replace(p2, "").replace(os.sep, "")

    def show_package_src(self, src):
        p1 = os.path.relpath(src, lp2_pkg_install_prefix)
        p = p1.split(os.sep)[0]
        return p
        
    def linked(self, src, dst):
        if not os.path.exists(dst):
            return False
        src_from_src = os.path.normpath(src)
        src_from_dst = os.path.abspath(os.readlink(dst))
        if src_from_src == src_from_dst:
            return True
        return False

    def unlink(self, src, dst):
        if not self.linked(src, dst):
            pkg_msg("%s does not come from %s and not removed"
                    % (warn(dst),
                       warn(self.show_package_src(src))))
            return
        os.remove(dst)
