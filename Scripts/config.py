#!/usr/bin/env python

import os, multiprocessing

cores = multiprocessing.cpu_count()
home = os.environ["HOME"]
lp2_config_path = os.path.dirname(os.path.abspath(__file__))
lp2_prefix = os.path.normpath(os.path.join(lp2_config_path, "../"))
lp2_pkg_script_prefix = os.path.join(lp2_prefix, "Scripts", "Recipes")
lp2_pkg_build_prefix = os.path.join(lp2_prefix, "Kitchen")
lp2_pkg_install_prefix = os.path.join(lp2_prefix, "Dishes")
lp2_pkg_active_prefix = os.path.join(lp2_prefix)
