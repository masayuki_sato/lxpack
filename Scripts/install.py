#!/usr/bin/env python

import os, sys
import package
from console import show_packages, green

from dependency import DependencyExtractor
from package_manipulator import Installer

def show_depend_install(depend):
    depend_installed = set()
    for x in depend:
        if not package.get(x).installed():
            depend_installed.add(x)
    if len(depend_installed):
        m = "installed because of dependencies"
        show_packages(depend_installed, msg=m, color=green)

def install(args):    
    target, depend = DependencyExtractor(args.packages).extract()
    show_depend_install(depend)
    Installer(args.packages).install()

if __name__=="__main__":
    pass
