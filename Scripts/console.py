#!/usr/bin/env python

term = "\033[0m"

def green(letters):
    return '\033[92m' + letters + term

def blue(letters):
    return '\033[94m' + letters + term

def yellow(letters):
    return '\033[93m' + letters + term

def red(letters):
    return '\033[91m' + letters + term

def ok(letters):
    return green(letters)

def note(letters):
    return blue(letters)

def warn(letters):
    return yellow(letters)

def error(letters):
    return red(letters)

def message(letters):
    print(letters)

def pkg_msg(letters):
    print(blue("--> ") + letters)
    
def pp_package(pack_list, colorfunc):
    line = ""
    for x in pack_list:
        line += colorfunc(x) if colorfunc else x
        line += ", "
    line = line[:-2]
    return line

message_set = set()

def message_once(letters):
    if letters in message_set:
        return
    message(letters)
    message_set.add(letters)

def show_packages(package_list, msg=None, color=None):
    if msg:
        print(msg + ": " + pp_package(package_list, color))
        return
    print(pp_package(package_list, color))
        
def show_packages_with_message(message, package_list, color=None):
    print(message + ": " + pp_package(package_list, color))
