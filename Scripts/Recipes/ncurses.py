#!/usr/bin/env python

from package import Package

class Ncurses(Package):
    src_url = "http://ftp.gnu.org/gnu/ncurses/ncurses-6.0.tar.gz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        config_opts = ["--enable-widec",
                       "--with-shared",
                       "--without-ada",
                       "--enable-safe-sprintf",
                       "--enable-sigwinch",
                       "--enable-ext-colors"]
        self.configure(opts=config_opts)
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
