#!/usr/bin/env python

from package import Package

class Mercurial(Package):
    src_url = "https://www.mercurial-scm.org/release/mercurial-4.3.1.tar.gz"
    depends_on = ["python"]

    def pre_install(self):
        pass

    def install(self):
        self.execute(["python", "setup.py", "install", "--prefix=%s" % self.prefix])

    def post_install(self):
        pass
