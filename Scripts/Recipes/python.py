#!/usr/bin/env python

from package import Package
import os.path

class Python(Package):
    src_url = "https://www.python.org/ftp/python/2.7.13/Python-2.7.13.tar.xz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure(["--enable-shared", "--enable-static"])
        self.make()
        self.make(opts=["install"])
        self.execute([ os.path.join(self.prefix, 'bin', 'python'),
                       '-m',
                       'ensurepip'
                       ])

    def post_install(self):
        pass
