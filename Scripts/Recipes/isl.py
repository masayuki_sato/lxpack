#!/usr/bin/env python

from package import Package

class Isl(Package):
    src_url = "http://isl.gforge.inria.fr/isl-0.18.tar.xz"
    depends_on = ["gmp"]

    def pre_install(self):
        pass

    def install(self):
        self.configure(["--with-gmp-prefix=%s" % self.dependencies["gmp"].prefix])
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
