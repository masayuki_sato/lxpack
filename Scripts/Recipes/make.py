#!/usr/bin/env python

from package import Package

class Make(Package):
    src_url = "https://ftp.gnu.org/gnu/make/make-4.2.tar.gz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
