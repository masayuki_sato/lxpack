#!/usr/bin/env python

from package import Package

class Gmp(Package):
    src_url = "https://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.xz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
