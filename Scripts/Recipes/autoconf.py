#!/usr/bin/env python

from package import Package

class Autoconf(Package):
    src_url = "https://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
