#!/usr/bin/env python

from package import Package

class Libffi(Package):
    src_url = "ftp://sourceware.org/pub/libffi/libffi-3.2.1.tar.gz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
