#!/usr/bin/env python

from package import Package

class Crosstool(Package):
    src_url = "http://crosstool-ng.org/download/crosstool-ng/crosstool-ng-1.23.0.tar.xz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
