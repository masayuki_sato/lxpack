#!/usr/bin/env python

from package import Package

class Mpc(Package):
    src_url = "ftp://ftp.gnu.org/gnu/mpc/mpc-1.0.3.tar.gz"
    depends_on = ["gmp", "mpfr"]

    def pre_install(self):
        pass

    def install(self):
        config_opts = [ "--with-gmp=%s" % self.dependencies["gmp"].prefix,
                       "--with-mpfr=%s" % self.dependencies["mpfr"].prefix ]
        self.configure(config_opts)
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
