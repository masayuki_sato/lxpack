#!/usr/bin/env python

from package import Package

class Libcerf(Package):
    src_url = "http://apps.jcns.fz-juelich.de/src/libcerf/libcerf-1.5.tgz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
