#!/usr/bin/env python

from package import Package

class Protobuf(Package):
    src_url = "https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.bz2"
    depends_on = ["python"]

    def pre_install(self):
        pass

    def install(self):
        self.execute(['./autogen.sh'])
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
