#!/usr/bin/env python

from package import Package

class Scons(Package):
    src_url = "http://prdownloads.sourceforge.net/scons/scons-2.5.1.tar.gz"
    #src_url = "http://prdownloads.sourceforge.net/scons/scons-3.0.1.tar.gz"
    depends_on = ["python"]

    def pre_install(self):
        pass

    def install(self):
        #self.configure()
        #self.make()
        #self.make(opts=["install"])
        self.execute(["python", "setup.py", "install", "--prefix=%s" % self.prefix])

    def post_install(self):
        pass
