#!/usr/bin/env python

from package import Package
import multiprocessing

class Gcc55(Package):
    src_url = "http://ftp.gnu.org/gnu/gcc/gcc-5.5.0/gcc-5.5.0.tar.xz"
    depends_on = ["gmp", "mpfr", "mpc", "isl", "binutils"]

    def pre_install(self):
        pass

    def install(self):
        self.makedirs("./build")
        self.chdir("./build")
        config_cmd = [ "../configure",
                       "--prefix=%s" % self.prefix,
                       "--enable-languages=c,c++,fortran,lto",
                       "--disable-multilib",
                       "--with-gmp=%s" % self.dependencies["gmp"].prefix,
                       "--with-mpfr=%s" % self.dependencies["mpfr"].prefix,
                       "--with-mpc=%s" % self.dependencies["mpc"].prefix,
                       "--with-isl=%s" % self.dependencies["isl"].prefix,              
                       "--program-suffix=55"
        ]
        self.execute(config_cmd)
        self.execute(["unset LIBRARY_PATH;", "make", "-j", str(multiprocessing.cpu_count())])
        self.make(opts=["install"])

    def post_install(self):
        pass
