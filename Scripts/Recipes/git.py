#!/usr/bin/env python

from package import Package

class Git(Package):
    src_url = "https://www.kernel.org/pub/software/scm/git/git-2.14.1.tar.xz"
    depends_on = ["curl"]

    def pre_install(self):
        pass

    def install(self):
        config_opts = [ "--with-curl=%s" % self.dependencies["curl"].prefix ]
        self.configure(config_opts)
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
