#!/usr/bin/env python

from package import Package

class Emacs(Package):
    src_url = "http://ftp.jaist.ac.jp/pub/GNU/emacs/emacs-25.3.tar.xz"
    src_sha256 = "253ac5e7075e594549b83fd9ec116a9dc37294d415e2f21f8ee109829307c00b"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        config_opts = [ "--with-xpm=no",
                        "--with-gif=no",
                        "--with-tiff=no" ]
        self.configure(opts=config_opts)
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
