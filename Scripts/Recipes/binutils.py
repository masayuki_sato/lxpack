#!/usr/bin/env python

from package import Package

class Binutils(Package):
    src_url = "http://ftp.gnu.org/gnu/binutils/binutils-2.29.tar.xz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
