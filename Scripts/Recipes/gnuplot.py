#!/usr/bin/env python

from package import Package

class Gnuplot(Package):
    src_url = "https://jaist.dl.sourceforge.net/project/gnuplot/gnuplot/5.2.0/gnuplot-5.2.0.tar.gz"
    depends_on = ["libcerf"]

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
