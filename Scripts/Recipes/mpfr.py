#!/usr/bin/env python

from package import Package

class Mpfr(Package):
    src_url = "https://ftp.gnu.org/gnu/mpfr/mpfr-3.1.6.tar.xz"
    depends_on = ["gmp"]

    def pre_install(self):
        pass

    def install(self):
        config_opts = [ "--with-gmp=%s" % self.dependencies["gmp"].prefix ]
        self.configure(config_opts)
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
