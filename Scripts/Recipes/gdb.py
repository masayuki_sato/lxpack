#!/usr/bin/env python

from package import Package

class Gdb(Package):
    src_url = "http://ftp.gnu.org/gnu/gdb/gdb-7.12.tar.xz"
    depends_on = ["gcc", "python", "texinfo"]

    def pre_install(self):
        pass

    def install(self):
        self.configure([ "--with-python=%s" % self.dependencies["python"].prefix ])
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
