#!/usr/bin/env python

from package import Package

class Pcre(Package):
    src_url = "https://ftp.pcre.org/pub/pcre/pcre-8.40.tar.bz2"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
