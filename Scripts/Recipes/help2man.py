#!/usr/bin/env python

from package import Package

class Help2man(Package):
    src_url = "http://mirrors.ocf.berkeley.edu/gnu/help2man/help2man-1.47.5.tar.xz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
