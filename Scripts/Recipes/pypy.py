#!/usr/bin/env python

from package import Package
import multiprocessing

class Pypy(Package):
    src_url = "https://bitbucket.org/pypy/pypy/downloads/pypy3-v5.10.0-src.tar.bz2"
    depends_on = ['python', 'libffi', 'expat']

    def pre_install(self):
        pass

    def install(self):
        self.execute(["pip install pycparser"])
        self.chdir("./pypy/goal")
        config_cmd = [ "python",
                       "../../rpython/bin/rpython", 
                       "-Ojit",
                       "targetpypystandalone"
        ]
        self.execute(config_cmd)

    def post_install(self):
        pass
