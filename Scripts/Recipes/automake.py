#!/usr/bin/env python

from package import Package

class Automake(Package):
    src_url = "https://ftp.gnu.org/gnu/automake/automake-1.15.tar.xz"
    depends_on = ["autoconf"]

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
