#!/usr/bin/env python

from package import Package

class Libgd(Package):
    src_url = "https://github.com/libgd/libgd/archive/gd-2.2.5.tar.gz"
    depends_on = ["autoconf"]

    def pre_install(self):
        pass

    def install(self):
        self.execute(["./bootstrap.sh"])
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
