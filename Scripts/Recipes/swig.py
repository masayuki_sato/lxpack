#!/usr/bin/env python

from package import Package

class Swig(Package):
    src_url = "http://prdownloads.sourceforge.net/swig/swig-3.0.12.tar.gz"
    depends_on = ["pcre"]

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
