#!/usr/bin/env python

from package import Package

class Screen(Package):
    src_url = "http://ftp.gnu.org/gnu/screen/screen-4.5.1.tar.gz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure(["--enable-colors256"])
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
