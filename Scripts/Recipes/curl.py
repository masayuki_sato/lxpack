#!/usr/bin/env python

from package import Package

class Curl(Package):
    src_url = "https://curl.haxx.se/download/curl-7.54.0.tar.gz"
    depends_on = []

    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(opts=["install"])

    def post_install(self):
        pass
