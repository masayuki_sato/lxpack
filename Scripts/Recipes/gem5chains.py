#!/usr/bin/env python

from package import Package

class Gem5chains(Package):
    src_url = None
    depends_on = ["binutils", "gcc", "python", "scons", "swig", "mercurial", "gperftools", "protobuf", "gdb"]

    def pre_install(self):
        pass

    def install(self):
        pass

    def post_install(self):
        pass
