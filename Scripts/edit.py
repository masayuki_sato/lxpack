#!/usr/bin/env python

import os, sys
import package
from config import lp2_pkg_script_prefix

def edit(args):
    recipe = os.path.join(lp2_pkg_script_prefix, args.package[0] + ".py")
    os.system("emacs %s" % recipe)

if __name__=="__main__":
    pass
