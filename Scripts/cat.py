#!/usr/bin/env python

import os, sys
import package
from config import lp2_pkg_script_prefix

def cat(args):
    recipe = os.path.join(lp2_pkg_script_prefix, args.package[0] + ".py")
    os.system("cat %s" % recipe)

if __name__=="__main__":
    pass
