#!/usr/bin/env python

import os, sys

import package
from console import show_packages, red
from dependency import ReverseDependencyExtractor
from package_manipulator import Uninstaller, Cleaner

def show_depend_uninstall(depend):
    depend_uninstalled = set()
    for x in depend:
        if package.get(x).installed():
            depend_uninstalled.add(x)
    if len(depend_uninstalled):
        m = "uninstalled because of dependencies"
        show_packages(depend_uninstalled, msg=m, color=red)
        m = "Are you sure to uninstall all the dependencies? (yes/no) > "
        if raw_input(m) != "yes":
            sys.exit(0)

def uninstall(args):
    targets, redeps = ReverseDependencyExtractor(args.packages).extract()
    show_depend_uninstall(redeps)
    Uninstaller(targets).uninstall()
    Cleaner(targets).clean()

if __name__=="__main__":
    pass
