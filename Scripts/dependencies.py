#!/usr/bin/env python

import os, sys
import package

from dependency import DependencyExtractor, ReverseDependencyExtractor
from console import show_packages, green

def dependencies(args):
    package_name = args.package[0]
    package, dep_set = DependencyExtractor([package_name]).extract()
    package, rev_dep_set = ReverseDependencyExtractor([package_name]).extract()
    show_packages(dep_set, msg="dependencies", color=green)
    show_packages(rev_dep_set, msg="reverse dependencies",  color=green)
    
if __name__=="__main__":
    pass
