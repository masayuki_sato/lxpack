#!/usr/bin/env python

import os, sys
import package

def installed(args):
    for name in package.all_iterator():
        p = package.get(name)
        if p.installed():
            print(name)

def activated(args):
    for name in package.all_iterator():
        p = package.get(name)
        if p.activated():
            print(name)

if __name__=="__main__":
    pass
