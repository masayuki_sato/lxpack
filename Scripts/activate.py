#!/usr/bin/env python

import package
from console import show_packages, green
from dependency import DependencyExtractor
from package_manipulator import Activator

def show_depend_activate(depend):
    depend_activated = set()
    for x in depend:
        if not package.get(x).activated():
            depend_activated.add(x)
    if len(depend_activated):
        m = "activated because of dependencies"
        show_packages(depend_activated, msg=m, color=green)

def activate(args):
    target, depend = DependencyExtractor(args.packages).extract()
    show_depend_activate(depend)
    Activator(args.packages).activate()

if __name__=="__main__":
    pass
