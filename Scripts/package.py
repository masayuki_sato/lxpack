#!/usr/bin/env python

import os, sys, errno, tarfile
from os import system

from config import cores, lp2_prefix, lp2_pkg_script_prefix, lp2_pkg_build_prefix, lp2_pkg_install_prefix, lp2_pkg_active_prefix
sys.path.append(lp2_pkg_script_prefix)

from file_manip import FileManipulator
from package_flags import PackageFlags
from console import pkg_msg

class Dependencies(object):

    def __init__(self, dep_list):
        self.dependencies = set()
        for x in dep_list:
            self.add(x)

    def add(self, name):
        self.dependencies.add(name)

    def package_generator(self):
        for x in self.dependencies:
            yield get_packager(x)

    def __getitem__(self, name):
        return get_packager(name)

    def get(self):
        d = set()
        for x in self.package_generator():
            d = d | x.get_dependencies()
        d = self.dependencies | d
        return d

class HashChecker(object):

    def __init__(self, filename, hash_val):
        self.filename = filename
        self.hash_val = hash_val
    
    def sha256_checksum(filename, block_size=65536):
        sha256 = hashlib.sha256()
        with open(filename, 'rb') as f:
            for block in iter(lambda: f.read(block_size), b''):
                sha256.update(block)
        return sha256.hexdigest()        

class PackageProxy(object):

    packages = {}
            
    def __getitem__(self, key):
        if not key in self.packages:
            self.packages[key] = get(key)
        return self.packages[key]

class Package(FileManipulator):
    src_url = ""
    src_sha256 = ""
    depends_on = []
    ignore_active = []
    ignore_active_base = [ "share/info/dir",
                           "LXPACK.installed",
                           "LXPACK.activated" ]
    
    def __init__(self, name=None):
        self.set_name(name)
        self.set_prefix()
        self.set_build_prefix()
        self.set_src_ball_path()
        self.set_src_path()
        self.set_log_path()
        self.dependencies = Dependencies(self.depends_on)
        self.flags = PackageFlags(self.prefix, self.build_prefix)
        self.package = PackageProxy()

    def set_name(self, name):
        if name:
            self.name = name
            return
        self.name = self.__class__.__name__.lower()

    def set_prefix(self):
        self.prefix = os.path.join(lp2_pkg_install_prefix, self.name)

    def set_build_prefix(self):
        self.build_prefix = os.path.join(lp2_pkg_build_prefix, self.name)
                            
    def set_src_ball_path(self):
        if not self.src_url:
            return
        w = self.src_url.split("/")
        filename = w[-1]
        self.src_ball_path = os.path.join(self.build_prefix, filename)

    def set_src_path(self):
        self.src_path = os.path.join(self.build_prefix, "src")

    def set_log_path(self):
        self.log_path = os.path.join(self.build_prefix, "build.log")
        
    def get_dependencies(self):
        return self.dependencies.get()

    def get_depends_on(self):
        return self.depends_on

    def has_src(self):
        return True if self.src_url else False
    
    def execute(self, args, msg=None):
        cmd = ""
        for x in args:
            cmd = cmd + x + " "
        pkg_msg(msg if msg else cmd)
        cmd = cmd + " >> %s 2>&1" % self.log_path
        ret = os.system(cmd)
        if ret:
            pkg_msg("Error: see %s" % self.log_path)
            sys.exit(1)
        
    def download(self):
        self.makedirs( self.build_prefix )
        if os.path.exists( self.src_ball_path ):
            pkg_msg( "Skip donwload. %s already exists." % self.src_ball_path )
            return
        self.execute( ["wget", self.src_url],
                      msg="Download a source tarball from %s" % self.src_url )

    def checksum(self):
        pass
        
    def extract_src(self):
        self.makedirs( self.src_path )
        self.chdir( self.build_prefix )
        self.download()
        self.execute( ["tar", "xvf", self.src_ball_path,
                       "-C", self.src_path, "--strip-components", "1"],
                      msg="Extract the downloaded source tar ball to %s" % self.src_path )
        self.chdir( self.src_path )
        
    def pre_install(self):
        pass

    def install(self):
        self.configure()
        self.make()
        self.make(["install",])

    def autogen(self):
        self.execute(["sh", "./autogen.sh"])
        
    def configure(self, opts=[]):
        configure = [ "./configure", "--prefix=%s" % self.prefix ] + opts
        self.execute(configure)

    def make(self, opts=[]):
        make = ["make", "-j", str(cores)] + opts
        self.execute(make)

    def post_install(self):
        pass

    def installed(self):
        return self.flags.installed()

    def activated(self):
        return self.flags.activated()

    def cleaned(self):
        return self.flags.cleaned()
    
    def is_ignore_active(self, filename):
        for x in self.ignore_active_base + self.ignore_active:
            if filename.count(x) >= 1:
                return True
        return False

    def generate_linktuple(self):
        if hasattr(self, "linktuple"):
            return
        self.linktuple = []
        for root, dirs, files in os.walk(self.prefix):
            for pkg_file in files:
                src = os.path.join( root, pkg_file )
                relative_path = os.path.relpath(src, start=self.prefix)
                dst = os.path.join(lp2_pkg_active_prefix, relative_path)
                if self.is_ignore_active(relative_path):
                    continue
                self.linktuple.append( (src, dst) )

    def activate(self):
        self.generate_linktuple()
        for src, dst in self.linktuple:
            if self.linked(src, dst):
                continue
            self.link(src, dst)
        self.flags.activate()
    
    def deactivate(self):
        self.generate_linktuple()
        for src, dst in self.linktuple:
            self.unlink(src, dst)
        self.flags.deactivate()

    def uninstall(self):
        import shutil
        shutil.rmtree(self.prefix)

    def clean(self):
        import shutil
        shutil.rmtree(self.build_prefix)


def get_packager(package_name):
    module_name = package_name
    class_name = package_name.split(".")[-1].capitalize()
    try:
        exec "from Recipes.%s import %s" % (module_name, class_name)
    except ImportError:
        print("There is no recipe or no class in the recipe, called %s" % package_name)
        sys.exit(1)
    exec "packager = %s(name='%s')" % (class_name, package_name)
    return packager


def get(name):
    return get_packager(name)


def all_iterator():
    import os.path, pkgutil, Recipes
    pkgpath = os.path.dirname(Recipes.__file__)
    for loader, name, ispkg in pkgutil.walk_packages([pkgpath]):
        if not ispkg:
            yield name

if __name__=="__main__":
    pp = PackageProxy()
    print pp['gmp'].prefix
