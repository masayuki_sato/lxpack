#!/usr/bin/env python

import argparse

import os, sys
script_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "Scripts")
sys.path.append(script_path)

import uninstall
parser = argparse.ArgumentParser(prog="lxpack", description="LXPACK: A package manager for LX")
subparsers = parser.add_subparsers(help="sub-command help")

import install
parser_install = subparsers.add_parser("install", help="install packages")
parser_install.add_argument("packages", metavar="PACKAGE", nargs="+",
                            help="list of installed packages")
parser_install.set_defaults(func=install.install)

import uninstall
parser_uninstall = subparsers.add_parser("uninstall", help="uninstall packages")
parser_uninstall.add_argument("packages", metavar="PACKAGE", nargs="+",
                              help="list of installed packages")
parser_uninstall.set_defaults(func=uninstall.uninstall)

import clean
parser_clean = subparsers.add_parser("clean", help="clean build files of packages")
parser_clean.add_argument("packages", metavar="PACKAGE", nargs="+",
                          help="list of cleaned packages")
parser_clean.set_defaults(func=clean.clean)

import edit
parser_edit = subparsers.add_parser("edit", help="edit the recipe of the package")
parser_edit.add_argument("package", metavar="PACKAGE", nargs=1,
                         help="specify a package")
parser_edit.set_defaults(func=edit.edit)

import cat
parser_cat = subparsers.add_parser("cat", help="cat the recipe of the package")
parser_cat.add_argument("package", metavar="PACKAGE", nargs=1,
                         help="specify a package")
parser_cat.set_defaults(func=cat.cat)

import activate
parser_activate = subparsers.add_parser("activate", help="activate packages")
parser_activate.add_argument("packages", metavar="PACKAGE", nargs="+",
                              help="list of activated packages")
parser_activate.set_defaults(func=activate.activate)

import deactivate
parser_deactivate = subparsers.add_parser("deactivate", help="deactivate packages")
parser_deactivate.add_argument("packages", metavar="PACKAGE", nargs="+",
                              help="list of deactivate packages")
parser_deactivate.set_defaults(func=deactivate.deactivate)

import installed
parser_installed = subparsers.add_parser("installed", help="show installed packages")
parser_installed.set_defaults(func=installed.installed)

import installed
parser_activated = subparsers.add_parser("activated", help="show activated packages")
parser_activated.set_defaults(func=installed.activated)

import listpackages
parser_list = subparsers.add_parser("list", help="show all packages")
parser_list.set_defaults(func=listpackages.listing)

import search
parser_search = subparsers.add_parser("search", help="search packages")
parser_search.add_argument("words", metavar="WORD", nargs="+",
                           help="word to search")
parser_search.set_defaults(func=search.search)

import dependencies
parser_depend = subparsers.add_parser("dependencies", help="show packages depending on the specified package")
parser_depend.add_argument("package", metavar="PACKAGE", nargs=1,
                         help="specify a package")
parser_depend.set_defaults(func=dependencies.dependencies)

args = parser.parse_args()
args.func(args)
